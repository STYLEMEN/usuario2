
//Crear variable games con registros
var espanol = [
    {
      "categoria":["FOL"],
      "idioma":"esp",
      "validacion": 1,
      "nivel":"medio",
      "imagen":"/img/fol1.jpg",
      "pregunta":[
          {
          "enunciado":"Para que se produzca un incendio es necesario...",
          "respuestas":[
              {
              "a" : ["Un combustible, un comburente y una energía de activación", 0],
              "b" : ["Una comburente, una energía de activación y una reacción en cadena", 0],
              "c" : ["Ambas son correctas", 0],
              "d" : ["Ninguna es correcta", 1]
              }
          ],
          "explicacion":"No siempre es necesario un combustible, un comburente y la energía de activación pueden ser suficiente para que haya un incendio."
        }
      ] 
    },
    {
        "categoria":["FOL"],
        "idioma":"esp",
        "validacion": 1,
        "nivel":"facil",
        "imagen":"/img/fol2.jpg",
        "pregunta":[
            {
            "enunciado":"Respecto a los EPI, no es responsabilidad del empresario...",
            "respuestas":[
                {
                "a" : ["Vigilar por su uso efectivo y comprobar que son utilizados adecuadamente", 0],
                "b" : ["Facilitar a los trabajadores de manera gratuita los equipos de protección", 0],
                "c" : ["Colocar el EPI después de su uso en el lugar establecido para ello", 1],
                "d" : ["Todas son correctas", 0]
                }
            ],
            "explicacion":"Vigilar y comprobar que todo empleado utiliza el EPI de forma adecuada y facilitar el mismo es tarea del empresario, por otro lado, colocarlo en el sitio adecuado es tarea del propio empleado."
          }
        ] 
    },
    {
        "categoria":["FOL"],
        "idioma":"esp",
        "validacion": 1,
        "nivel":"medio",
        "imagen":"/img/fol3.jpg",
        "pregunta":[
            {
            "enunciado":"¿Qué son las condiciones de seguridad inadecuadas?",
            "respuestas":[
                {
                "a" : ["Utilizar equipos de trabajo inadecuado", 0],
                "b" : ["Aquellos factores de risgo que pueden originar los accidentes laborales", 1],
                "c" : ["Cualquier situacion que podemos observar en el mundo laboral", 0],
                "d" : ["Ninguna es correcta", 0]
                }
            ],
            "explicacion":"Las condiciones de seguridad inadecuadas son aquellos factores de riesgo que pueden originar los accidentes laborales."
          }
        ] 
    },
    {
        "categoria":["FOL"],
        "idioma":"esp",
        "validacion": 1,
        "nivel":"facil",
        "imagen":"/img/fol4.jpg",
        "pregunta":[
            {
            "enunciado":"Factores de riesgo derivados de los agentes físicos:",
            "respuestas":[
                {
                "a" : ["Radiaciones", 0],
                "b" : ["Ruido", 0],
                "c" : ["Temperatura", 0],
                "d" : ["Todas son correctas", 1]
                }
            ],
            "explicacion":"Todas estas energías son factores de riesgo para los lugares de trabajo."
          }
        ] 
    },
    {
        "categoria":["FOL"],
        "idioma":"esp",
        "validacion": 1,
        "nivel":"facil",
        "imagen":"/img/fol5.jpg",
        "pregunta":[
            {
            "enunciado":"En la señalización de seguridad la forma triangular significa:",
            "respuestas":[
                {
                "a" : ["Obligación", 0],
                "b" : ["Advertencia", 1],
                "c" : ["Salvamento", 0],
                "d" : ["Prohibición", 0]
                }
            ],
            "explicacion":"En la señalización de seguridad la forma triangular significa: Advertencia de peligro."
          }
        ] 
    },
    {
        "categoria":["FOL"],
        "idioma":"esp",
        "validacion": 1,
        "nivel":"facil",
        "imagen":"/img/fol6.jpg",
        "pregunta":[
            {
            "enunciado":"Las medidas que tratan de eliminar o reducir el riesgo en su punto de origen se donominan: ",
            "respuestas":[
                {
                "a" : ["Medidas de prevención", 1],
                "b" : ["Medidas de protección", 0],
                "c" : ["Medidas de reducción", 0],
                "d" : ["Todas son incorrectas", 0]
                }
            ],
            "explicacion":"Dichas medidas se donominan: medidas de prevención."
          }
        ] 
    },
    {
        "categoria":["FOL"],
        "idioma":"esp",
        "validacion": 1,
        "nivel":"medio",
        "imagen":"/img/fol7.jpg",
        "pregunta":[
            {
            "enunciado":"¿Que recibe el nombre de equipo de trabajo?",
            "respuestas":[
                {
                "a" : ["Cualquier máquina, herramienta o instalación o vehículo de transporte utilizado en el trabajo", 1],
                "b" : ["Exclusivamente las máquinas o herramientas utilizadas en el puesto de trabajo", 0],
                "c" : ["Solo las instalaciones y vehículos de transporte utilizados", 0],
                "d" : ["Ninguna es correcta", 0]
                }
            ],
            "explicacion":"Todo lo utilizado para desempeñar el trabajo se denomina equipo de trabajo."
          }
        ] 
    },
    {
        "categoria":["FOL"],
        "idioma":"esp",
        "validacion": 1,
        "nivel":"medio",
        "imagen":"/img/fol8.jpg",
        "pregunta":[
            {
            "enunciado":"La enfermedad profesional se puede definir como:",
            "respuestas":[
                {
                "a" : ["Daño derivado del trabajo", 1],
                "b" : ["Una consecuencia derivada únicamente de los productos o actividades potencialmente peligrosos", 0],
                "c" : ["La posibilidad que tiene el trabajador de sufrirla en su actividad laboral", 0],
                "d" : ["Una característica de los lugares de trabajo a los que todos están expuestos", 0]
                }
            ],
            "explicacion":"Una enfermedad Profesional es aquélla contraída a consecuencia del trabajo ejecutado por cuenta ajena o propia en las actividades que se especifiquen en el cuadro de Enfermedades Profesionales."
          }
        ] 
    },
    {
        "categoria":["FOL"],
        "idioma":"esp",
        "validacion": 1,
        "nivel":"dificil",
        "imagen":"/img/fol9.jpg",
        "pregunta":[
            {
            "enunciado":"¿Qué es la fatiga laboral?",
            "respuestas":[
                {
                "a" : ["Un accidente de trabajo", 0],
                "b" : ["Una enfermedad profesional", 0],
                "c" : ["Un riesgo laboral", 0],
                "d" : ["Un daño profesional", 1]
                }
            ],
            "explicacion":"La fatiga laboral es un problema que aparece en las personas como consecuencia de un trabajo prolongado, intenso y/o repetitivo y se considera daño profesional"
          }
        ] 
    },
    {
        "categoria":["FOL"],
        "idioma":"esp",
        "validacion": 1,
        "nivel":"dificil",
        "imagen":"/img/fol10.jpg",
        "pregunta":[
            {
            "enunciado":"La señalización de seguridad:",
            "respuestas":[
                {
                "a" : ["No es obligatoria", 0],
                "b" : ["Es una medida de protección colectiva", 1],
                "c" : ["Es un equipo de protección individual", 0],
                "d" : ["Es una medida preventiva", 0]
                }
            ],
            "explicacion":"Protección colectiva es aquella técnica de seguridad cuyo objetivo es la protección simultánea de varios trabajadores expuestos a un determinado riesgo."
          }
        ] 
    },
    {
        "categoria":["FOL"],
        "idioma":"esp",
        "validacion": 1,
        "nivel":"medio",
        "imagen":"/img/fol11.jpg",
        "pregunta":[
            {
            "enunciado":"La causa fundamental de los daños que se producen por la electricidad es:",
            "respuestas":[
                {
                "a" : ["El tiempo de contacto con la fuente", 0],
                "b" : ["El trayecto recorrido por el organismo", 0],
                "c" : ["La intensidad de la corriente", 1],
                "d" : ["La resistencia al paso de la corriente", 0]
                }
            ],
            "explicacion":"La causa fundamental de los daños los prudice la intensidad de la corriente."
          }
        ] 
    },
    {
        "categoria":["FOL"],
        "idioma":"esp",
        "validacion": 1,
        "nivel":"facil",
        "imagen":"/img/fol12.jpg",
        "pregunta":[
            {
            "enunciado":"¿Cuál es el lo primero que debe ser examinado en una persona que ha sufrido un accidente?",
            "respuestas":[
                {
                "a" : ["La consciencia", 1],
                "b" : ["Las fisuras", 0],
                "c" : ["Las hemorragias", 0],
                "d" : ["Las quemaduras", 0]
                }
            ],
            "explicacion":"La consciencia es el primer signo vital y debe ser examinado como prioridad."
          }
        ] 
    },
    {
        "categoria":["FOL"],
        "idioma":"esp",
        "validacion": 1,
        "nivel":"medio",
        "imagen":"/img/fol13.jpg",
        "pregunta":[
            {
            "enunciado":"Ante un accidentado inconsciente debemos:",
            "respuestas":[
                {
                "a" : ["Llamar a los servicios médicos", 1],
                "b" : ["Mover el cuello en busca de lesiones", 0],
                "c" : ["Darle agua", 0],
                "d" : ["Todas las anteriores", 0]
                }
            ],
            "explicacion":"Llamar a los servicios medicos es la primera medida de actuación."
          }
        ] 
    },
    {
        "categoria":["FOL"],
        "idioma":"esp",
        "validacion": 1,
        "nivel":"facil",
        "imagen":"/img/fol14.jpg",
        "pregunta":[
            {
            "enunciado":"Qué significan las siglas P.A.S?",
            "respuestas":[
                {
                "a" : ["proteger, ayudar y salvar", 0],
                "b" : ["Proteger, asistir y socorrer", 1],
                "c" : ["peligro, avisar y socorrer", 0],
                "d" : ["por favor, ayuda y salva", 0]
                }
            ],
            "explicacion":"P.A.S: Proteger, asistir y socorrer."
          }
        ] 
    },
    {
        "categoria":["FOL"],
        "idioma":"esp",
        "validacion": 1,
        "nivel":"facil",
        "imagen":"/img/fol15.jpg",
        "pregunta":[
            {
            "enunciado":"En primer lugar ¿a qué victimas hay que socorrer?",
            "respuestas":[
                {
                "a" : ["Mujeres y a los niños", 0],
                "b" : ["Personas incoscientes", 1],
                "c" : ["Menores de edad", 0],
                "d" : ["A los altos cargos del país", 0]
                }
            ],
            "explicacion":"Siempre a las victimas que puedan estar mas afectadas por el suceso."
          }
        ] 
    },
    {
        "categoria":["FOL"],
        "idioma":"esp",
        "validacion": 1,
        "nivel":"dificil",
        "imagen":"/img/fol16.jpg",
        "pregunta":[
            {
            "enunciado":"El Plan de Prevención es un documento...",
            "respuestas":[
                {
                "a" : ["Incluido dentro del plan de autoprotección", 0],
                "b" : ["Realizado cuando se produce un accidente de trabajo", 0],
                "c" : ["De obligado cumplimiento por las empresas", 1],
                "d" : ["Realizaso por las empresas de manera voluntaria", 0]
                }
            ],
            "explicacion":"De obligado cumplimiento por las empresas."
          }
        ] 
    },
    {
        "categoria":["FOL"],
        "idioma":"esp",
        "validacion": 1,
        "nivel":"media",
        "imagen":"/img/fol17.jpg",
        "pregunta":[
            {
            "enunciado":"¿En qué consiste la PLS?",
            "respuestas":[
                {
                "a" : ["Tumbar boca abajo al accidentado", 0],
                "b" : ["Estirarle las piernas, colocar el brazo más próximo en ángulo recto con el cuerpo y tumbado lateral ", 1],
                "c" : ["EStirarle las piernes, colocar boca arriba y empujar su cabeza hacia arriba", 0],
                "d" : ["Todas son correctas", 0]
                }
            ],
            "explicacion":"La PLS consiste en: estirarle las piernas, colocar el brazo más próximo en ángulo recto con el cuerpo y tumbado lateral."
          }
        ] 
    },
    {
        "categoria":["FOL"],
        "idioma":"esp",
        "validacion": 1,
        "nivel":"facil",
        "imagen":"/img/fol18.jpg",
        "pregunta":[
            {
            "enunciado":"¿Que quiere decir esta imagén?",
            "respuestas":[
                {
                "a" : ["Utilizar el ascensor en caso de incendio", 0],
                "b" : ["No utilizar el ascensor dos o mas personas", 0],
                "c" : ["No utilizar el ascensor en caso de emergencia", 1],
                "d" : ["Utilizar el ascensor siempre con dos personas", 0]
                }
            ],
            "explicacion":"La señal indica que no se debe utilizar el ascensor en caso de emergencia."
          }
        ] 
    },
    {
        "categoria":["FOL"],
        "idioma":"esp",
        "validacion": 1,
        "nivel":"facil",
        "imagen":"/img/fol19.jpg",
        "pregunta":[
            {
            "enunciado":"¿Qué significa RCP",
            "respuestas":[
                {
                "a" : ["Resucitación cardiaca personal", 0],
                "b" : ["Reanimación corporal posicional", 0],
                "c" : ["Reanimación cardio-pulmonar", 1],
                "d" : ["Resucitación cr7", 0]
                }
            ],
            "explicacion":"La reanimación cardiopulmonar (RCP) es una técnica que salva vidas, útil en muchas emergencias como por ejemplo un ataque cardíaco, en los que la respiración o los latidos del corazón de una persona se han detenido."
          }
        ] 
    },
    {
        "categoria":["FOL"],
        "idioma":"esp",
        "validacion": 1,
        "nivel":"facil",
        "imagen":"/img/fol20.jpg",
        "pregunta":[
            {
            "enunciado":"¿Qué es la fatiga laboral?",
            "respuestas":[
                {
                "a" : ["Un accidente de trabajo", 0],
                "b" : ["Una enfermedad profesional", 0],
                "c" : ["Un riesgo laboral", 0],
                "d" : ["un daño profesional", 1]
                }
            ],
             "explicacion":""
          }
        ] 
    },
    {
        "categoria":["FOL"],
        "idioma":"esp",
        "validacion": 1,
        "nivel":"facil",
        "imagen":"/img/fol21.jpg",
        "pregunta":[
            {
            "enunciado":" Un riesgo laboral es",
            "respuestas":[
                {
                "a" : ["Cualquier característica del trabajo que puede influir en que aparezca un accidente", 0],
                "b" : ["Un peligro grave para el trabajador/a", 0],
                "c" : ["La posibilidad de sufrir un daño por el trabajo", 1],
                "d" : ["Son ciertas todas.", 0]
                }
            ],
            "explicacion":"Se entiende como riesgo laboral a los peligros existentes en una profesión y tarea profesional concreta, así como en el entorno o lugar de trabajo, susceptibles de originar accidentes o cualquier tipo de siniestros que puedan provocar algún daño o problema de salud tanto físico como psicológico."
          }
        ] 
    },
    {
        "categoria":["FOL"],
        "idioma":"esp",
        "validacion": 1,
        "nivel":"facil",
        "imagen":"/img/fol22.jpg",
        "pregunta":[
            {
            "enunciado":"No es una técnica de protección colectiva",
            "respuestas":[
                {
                "a" : ["Las redes de seguridad", 0],
                "b" : ["La higiene industrial", 1],
                "c" : ["Las barandillas", 0],
                "d" : ["La ventilación general", 0]
                }
            ],
            "explicacion":"las redes de seguridad, las barandillas y la ventilacion son medidas de protección, no una técnica per se."
          }
        ] 
    },
    {
        "categoria":["FOL"],
        "idioma":"esp",
        "validacion": 1,
        "nivel":"medio",
        "imagen":"/img/fol23.jpg",
        "pregunta":[
            {
            "enunciado":"Indica quién está autorizado para paralizar totalmente el trabajo en un centro de trabajo",
            "respuestas":[
                {
                "a" : ["Solamente el empresario o empresaria", 0],
                "b" : ["Si no lo hace la empresa, lo pueden hacer los representantes de los trabajadores", 1],
                "c" : ["Ninguna es cierta", 0],
                "d" : ["Cualquier trabajador puede paralizar toda la empresa", 0]
                }
            ],
            "explicacion":"El trabajador tendrá derecho a interrumpir su actividad y abandonar el lugar de trabajo, en caso necesario, cuando considere que dicha actividad entraña un riesgo grave e inminente para su vida o su salud, pero no esta autorizado para paralizar totalmente el centro de trabajo."
          }
        ] 
    },
    {
        "categoria":["FOL"],
        "idioma":"esp",
        "validacion": 1,
        "nivel":"medio",
        "imagen":"/img/fol24.png",
        "pregunta":[
            {
            "enunciado":"No es un deber del trabajador o trabajadora",
            "respuestas":[
                {
                "a" : ["Formarse por su cuenta en materia de prevención de riesgos laborales", 1],
                "b" : ["Usar correctamente y con diligencia los equipos de trabajo", 0],
                "c" : ["Cooperar con la empresa", 0],
                "d" : ["Cumplir las normas de seguridad", 0]
                }
            ],
            "explicacion":"El uso correcto de los equipos de trabajo, la cooperación con la empresa y el cumplir las normas de seguridad SI son deberes del trabajador."
          }
        ] 
    },
    {
        "categoria":["FOL"],
        "idioma":"esp",
        "validacion": 1,
        "nivel":"dificil",
        "imagen":"/img/fol25.jpg",
        "pregunta":[
            {
            "enunciado":"La legislación básica en materia de Prevención de Riesgos laborales es",
            "respuestas":[
                {
                "a" : ["Ley 31/1995", 0],
                "b" : ["RD 39/1997", 0],
                "c" : ["RD 1311/2005", 0],
                "d" : ["Son correctas a y b", 1]
                }
            ],
            "explicacion":"La ley 31/1995, nace el 8 de noviembre de ese año y es aprobada en el real decreto 39/1997 a 17 de enero."
          }
        ] 
    },
    {
        "categoria":["FOL"],
        "idioma":"esp",
        "validacion": 1,
        "nivel":"facil",
        "imagen":"/img/fol26.jpeg",
        "pregunta":[
            {
            "enunciado":"No se consideran EPI",
            "respuestas":[
                {
                "a" : ["Los cascos anti ruido", 0],
                "b" : ["Las pantallas de soldadura", 0],
                "c" : [" Los equipos de salvamento y socorro", 1],
                "d" : ["Los tapones para los oídos", 0]
                }
            ],
            "explicacion":"los equipos de salvamento y socorro no forman parte de tal caategoria."
          }
        ] 
    },
    {
        "categoria":["FOL"],
        "idioma":"esp",
        "validacion": 1,
        "nivel":"medio",
        "imagen":"/img/fol27.jpg",
        "pregunta":[
            {
            "enunciado":"Entre las medidas técnicas y organizativas frente al ruido tiene prioridad por ir dirigida al foco",
            "respuestas":[
                {
                "a" : ["Entregar los EPIs cuando se esté expuesto a más de 85 dB", 0],
                "b" : ["Colocar pantallas para evitar la propagación del ruido", 0],
                "c" : ["Reconocimientos médicos cada 3 años si supera los 85 dB", 0],
                "d" : ["Sustituir la maquinaria ruidosa por otra más silenciosa", 1]
                }
            ],
            "explicacion":"La principal medida de prevencion y proteccion con respecto al ruido es la elección de equipos de trabajo que generen el menor ruido posible."
          }
        ] 
    },
    {
        "categoria":["FOL"],
        "idioma":"esp",
        "validacion": 1,
        "nivel":"facil",
        "imagen":"/img/fol28.jpg",
        "pregunta":[
            {
            "enunciado":"Las vibraciones pueden ser producidas por:",
            "respuestas":[
                {
                "a" : ["Martillos neumáticos o apisonadoras manuales", 0],
                "b" : ["Conducción de vehículos agrícolas", 0],
                "c" : ["Son ciertas a y b", 1],
                "d" : ["Ninguna es correcta", 0]
                }
            ],
            "explicacion":"Se consideran vibraciones los movimientos oscilatorios que al entrar en contacto con el cuerpo humano pueden provocar efectos negativos. Como los casos de la respuesta a y b."
          }
        ] 
    },
    {
        "categoria":["FOL"],
        "idioma":"esp",
        "validacion": 1,
        "nivel":"facil",
        "imagen":"/img/fol29.jpg",
        "pregunta":[
            {
            "enunciado":"Para manipular correctamente una carga:",
            "respuestas":[
                {
                "a" : ["Doblar las rodillas para agarrar la carga.", 1],
                "b" : ["Doblar la espalda manteniendo las piernas rectas.", 0],
                "c" : ["Hay que mantener los pies lo más juntos posibles.", 0],
                "d" : ["Son ciertas a y b.", 0]
                }
            ],
            "explicacion":"Para adaptar la postura de levantamiento es necesario Doblar las piernas manteniendo en todo momento la espalda derecha, y mantenerel mentón metido, sin flexionar demasiado las rodillas."
          }
        ] 
    },
    {
          "categoria":["FOL"],
          "idioma":"esp",
          "validacion": 1,
          "nivel":"medio",
          "imagen":"/img/fol30.jpg",
          "pregunta":[
              {
              "enunciado":"¿Cuál es el primer paso para actuar frente a una parada cardiorrespiratoria?",
              "respuestas":[
                  {
                  "a" : ["Comenzar la RCP.", 0],
                  "b" : ["Reconocimiento de la parada cardiorrespiratoria.", 1],
                  "c" : ["Conseguir un DESA.", 0],
                  "d" : ["Ninguna de las anteriores", 0]
                  }
              ],
              "explicacion":"Antes de realizar cualquier tipo de acción es necesario reconocer el estado del afectado."
            }
          ] 
    },
    {
          "categoria":["FOL"],
          "idioma":"esp",
          "validacion": 1,
          "nivel":"medio",
          "imagen":"/img/fol31.jpg",
          "pregunta":[
              {
              "enunciado":"¿Qué debemos hacer si un atragantado pierde la consciencia? ",
              "respuestas":[
                  {
                  "a" : ["Colocarlo decúbito supino y comenzar las maniobras de RCP.", 1],
                  "b" : ["Realizar la maniobra de Heimlich.", 0],
                  "c" : ["Esperar a la ayuda profesional", 0],
                  "d" : ["Intentar sacar el objeto con unas pinzas.", 0]
                  }
              ],
              "explicacion":"Ante una situación de perdida de consciencia la rcp es la maniobra correcta a realizar."
            }
          ] 
    },
    {
          "categoria":["FOL"],
          "idioma":"esp",
          "validacion": 1,
          "nivel":"medio",
          "imagen":"/img/fol32.jpg",
          "pregunta":[
              {
              "enunciado":"¿Qué debemos hacer si un atragantado pierde la consciencia? ",
              "respuestas":[
                  {
                  "a" : ["Depositarlo decúbito supino y comenzar las maniobras de RCP.", 1],
                  "b" : ["Realizar la maniobra de Heimlich.", 0],
                  "c" : ["Esperar a la ayuda profesional", 0],
                  "d" : ["Intentar sacar el objeto con unas pinzas.", 0]
                  }
              ],
              "explicacion":"Ante una situación de perdida de consciencia la rcp es la maniobra correcta a realizar."
            }
          ] 
    },
    {
          "categoria":["FOL"],
          "idioma":"esp",
          "validacion": 1,
          "nivel":"dificil",
          "imagen":"/img/fol33.jpg",
          "pregunta":[
              {
              "enunciado":"¿Por debajo de qué temperatura puede producirse parada cardiorrespiratoria?",
              "respuestas":[
                  {
                  "a" : ["26 grados", 0],
                  "b" : ["34 grados.", 0],
                  "c" : ["28 grados.", 1],
                  "d" : ["30 grados.", 0]
                  }
              ],
              "explicacion":"Por debajo de los 28ºC de temperatura central corporal se puede producir una parada cardiaca (fibrilación ventricular o asistolia)."
            }
          ] 
    }

] 
  //Conectar a la base de datos
  db = connect("localhost:27017/admin");
  
  //Introducir usuario y contraseña
  //db.auth('almi', 'Almi123');
  
  //Seleccionar la base de datos
  db = db.getSiblingDB('photoplay');
  
  //Insertar
  db.preguntas.insert(espanol);
  