
//Crear variable games con registros
var espanol = [
    {
        "categoria":["ingles"],
        "idioma":"eng",
        "validacion": 1,
        "nivel":"facil",
        "imagen":"/img/ingles16.jpg",
        "pregunta":[
            {
            "enunciado":"The smallest unit on a display screen is...",
            "respuestas":[
                {
                "a" : ["A pixel", 1],
                "b" : ["Resolution", 0],
                "c" : ["LCD", 1],
                "d" : ["A bit", 0]
                }
            ],
            "explicacion":"A pixel is the smallest unit on a display screen."
          }
        ] 
    },
    {
        "categoria":["ingles"],
        "idioma":"eng",
        "validacion": 1,
        "nivel":"medio",
        "imagen":"/img/ingles17.jpg",
        "pregunta":[
            {
            "enunciado":"To generate video signal on your PC you need...",
            "respuestas":[
                {
                "a" : ["video adapter", 1],
                "b" : ["video focus", 0],
                "c" : ["pixels", 0],
                "d" : ["display", 0]
                }
            ],
            "explicacion":"You need a video adapter to generate video signal on your computer."
          }
        ] 
    },
    {
        "categoria":["ingles"],
        "idioma":"eng",
        "validacion": 1,
        "nivel":"facil",
        "imagen":"/img/ingles18.jpg",
        "pregunta":[
            {
            "enunciado":"What is hardware?",
            "respuestas":[
                {
                "a" : ["The physical parts of the computer including the monitor, the keyboard, and the mouse.", 1],
                "b" : ["The operating system and other programs such as Microsoft Word and Microsoft Powerpoint.", 0],
                "c" : ["Neither", 0],
                "d" : ["Both", 0]
                }
            ],
            "explicacion":"The physical parts of the computer including the monitor, the keyboard, and the mouse."
          }
        ] 
    },
    {
        "categoria":["ingles"],
        "idioma":"eng",
        "validacion": 1,
        "nivel":"facil",
        "imagen":"/img/ingles19.jpg",
        "pregunta":[
            {
            "enunciado":"Which of these are software?",
            "respuestas":[
                {
                "a" : ["Trackball", 0],
                "b" : ["Scanner", 0],
                "c" : ["Internet Explorer", 1],
                "d" : ["Keyboard", 0]
                }
            ],
            "explicacion":"Internet Explorer is a software."
          }
        ] 
    },
    {
        "categoria":["ingles"],
        "idioma":"eng",
        "validacion": 1,
        "nivel":"dificil",
        "imagen":"/img/ingles20.jpg",
        "pregunta":[
            {
            "enunciado":"How much data is represented by 1,000,000,000 bytes?",
            "respuestas":[
                {
                "a" : ["1 Megabyte 1(MB)", 0],
                "b" : ["1 Gigabyte (1G)", 1],
                "c" : ["1 byte", 0],
                "d" : ["1 bit", 0]
                }
            ],
            "explicacion":"1 Gigabyte is 1,000,000,000 bytes."
          }
        ] 
    },
    {
        "categoria":["ingles"],
        "idioma":"eng",
        "validacion": 1,
        "nivel":"medio",
        "imagen":"/img/ingles21.jpg",
        "pregunta":[
            {
            "enunciado":"Which type of port is used to connect a peripheral device to the computer?",
            "respuestas":[
                {
                "a" : ["CD-ROM", 0],
                "b" : ["RAM", 0],
                "c" : ["ROM", 0],
                "d" : ["USB", 1]
                }
            ],
            "explicacion":"USB device is a peripheral."
          }
        ] 
    },
    {
        "categoria":["ingles"],
        "idioma":"eng",
        "validacion": 1,
        "nivel":"medio",
        "imagen":"/img/ingles22.png",
        "pregunta":[
            {
            "enunciado":"Which of the following devices can be classified as storage devices?",
            "respuestas":[
                {
                "a" : ["RAM, ROM, CD-ROM, and DVD", 0],
                "b" : ["ROM and RAM ", 0],
                "c" : ["CD-ROM, DVD, Floppy disk", 1],
                "d" : ["Word, Excel, and AOL", 0]
                }
            ],
            "explicacion":"CD-ROM, DVD, Floppy disk are storage devices."
          }
        ] 
    },
    {
        "categoria":["ingles"],
        "idioma":"eng",
        "validacion": 1,
        "nivel":"dificil",
        "imagen":"/img/ingles23.jpg",
        "pregunta":[
            {
            "enunciado":"Which of the following allows you to add additional memory to your computer?",
            "respuestas":[
                {
                "a" : ["Senders", 0],
                "b" : ["Read Only Memory CDs", 0],
                "c" : ["Expansion slots", 1],
                "d" : ["Motherboards", 0]
                }
            ],
            "explicacion":"Expansion slots like his name says, add expansion to the memory of the computer"
          }
        ] 
    },
    {
        "categoria":["ingles"],
        "idioma":"eng",
        "validacion": 1,
        "nivel":"medio",
        "imagen":"/img/ingles24.jpeg",
        "pregunta":[
            {
            "enunciado":"How many bits is one byte",
            "respuestas":[
                {
                "a" : ["7", 0],
                "b" : ["8", 1],
                "c" : ["9", 0],
                "d" : ["5", 0]
                }
            ],
            "explicacion":"One byte are 8 bits."
          }
        ] 
    },
    {
        "categoria":["ingles"],
        "idioma":"eng",
        "validacion": 1,
        "nivel":"dificil",
        "imagen":"/img/ingles25.jpg",
        "pregunta":[
            {
            "enunciado":"What is Bitrate?",
            "respuestas":[
                {
                "a" : ["The number of bits that can be transferred per second", 1],
                "b" : ["How fast the CPU is", 0],
                "c" : ["How fast the motherboard is", 0],
                "d" : ["All are false", 0]
                }
            ],
            "explicacion":"Bitrate is the number of bits that can be transferred per second"
          }
        ] 
    },
    {
        "categoria":["ingles"],
        "idioma":"eng",
        "validacion": 1,
        "nivel":"medio",
        "imagen":"/img/ingles26.jpg",
        "pregunta":[
            {
            "enunciado":"What does DDoS stand for?",
            "respuestas":[
                {
                "a" : ["Distributed Denial of Service", 1],
                "b" : ["Deny Digital on Server", 0],
                "c" : ["Digital Drive over Stands", 0],
                "d" : ["Driver Defeat Service", 0]
                }
            ],
            "explicacion":"DDoS: Distributed Denial of Service"
          }
        ] 
    },
    {
        "categoria":["ingles"],
        "idioma":"eng",
        "validacion": 1,
        "nivel":"facil",
        "imagen":"/img/ingles27.png",
        "pregunta":[
            {
            "enunciado":"What does IP stand for in IP address?",
            "respuestas":[
                {
                "a" : ["Internet Procedure", 0],
                "b" : ["Internet Policy", 0],
                "c" : ["Internet Password", 0],
                "d" : ["Internet protocol", 1]
                }
            ],
            "explicacion":"IP is: Internet protocol"
          }
        ] 
    },
    {
        "categoria":["ingles"],
        "idioma":"eng",
        "validacion": 1,
        "nivel":"medio",
        "imagen":"/img/ingles28.jpg",
        "pregunta":[
            {
            "enunciado":"What does the term GUI stand for? ",
            "respuestas":[
                {
                "a" : ["Graphics User Interface", 0],
                "b" : ["Graphical User Interaction ", 0],
                "c" : ["Graphical User Interface", 1],
                "d" : ["Graphics User Inrection", 0]
                }
            ],
            "explicacion":"GUI: Graphical User Interface"
          }
        ] 
    },
    {
        "categoria":["ingles"],
        "idioma":"eng",
        "validacion": 1,
        "nivel":"medio",
        "imagen":"/img/ingles29.png",
        "pregunta":[
            {
            "enunciado":"Yahoo, Google and Bing are all examples of: ",
            "respuestas":[
                {
                "a" : ["search engines", 1],
                "b" : ["search tools", 0],
                "c" : ["mind reading websites", 0],
                "d" : ["personal websites", 0]
                }
            ],
            "explicacion":"Google, bing and yahoo are search engines."
          }
        ] 
    },
    {
        "categoria":["ingles"],
        "idioma":"eng",
        "validacion": 1,
        "nivel":"dificil",
        "imagen":"/img/ingles30.jpg",
        "pregunta":[
            {
            "enunciado":"The monitor is?",
            "respuestas":[
                {
                "a" : ["similar to a TV screen", 1],
                "b" : ["similar to the mouse", 0],
                "c" : ["similar to the keyboard", 0],
                "d" : ["Is like a peace of peaper", 0]
                }
            ],
            "explicacion":"Monitor is: similar to a TV screen"
          }
        ] 
    },
    

    

] 
  //Conectar a la base de datos
  db = connect("localhost:27017/admin");
  
  //Introducir usuario y contraseña
  //db.auth('almi', 'Almi123');
  
  //Seleccionar la base de datos
  db = db.getSiblingDB('photoplay');
  
  //Insertar
  db.preguntas.insert(espanol);

