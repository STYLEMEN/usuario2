
//Crear variable games con registros
var espanol = [
    {
        "categoria":["lenguaje"],
        "idioma":"esp",
        "validacion": 1,
        "nivel":"facil",
        "imagen":"/img/lenguaje1.jpg",
        "pregunta":[
            {
            "enunciado":"¿Qué entiende por HTML?",
            "respuestas":[
                {
                "a" : ["Hyper Text Mask Language", 0],
                "b" : ["Hard Text Markup Language", 0],
                "c" : ["Hyper Text Markup Language", 1],
                "d" : ["Hiper Type Markup Language ", 0]
                }
            ],
            "explicacion":"Las siglas HTML significan: Hyper Text Markup Language"
          }
        ] 
    },
    {
        "categoria":["lenguaje"],
        "idioma":"esp",
        "validacion": 1,
        "nivel":"facil",
        "imagen":"/img/lenguaje2.png",
        "pregunta":[
            {
            "enunciado":"¿Qué etiqueta utilizamos para insertar una línea horizontal? ",
            "respuestas":[
                {
                "a" : ["br", 0],
                "b" : ["hr", 1],
                "c" : ["line", 0],
                "d" : ["wbr", 0]
                }
            ],
            "explicacion":"La etiqueta a utilizar en caso de querer introducir una linea es: 'hr'"
          }
        ] 
    },
    {
        "categoria":["lenguaje"],
        "idioma":"esp",
        "validacion": 1,
        "nivel":"medio",
        "imagen":"/img/lenguaje3.png",
        "pregunta":[
            {
            "enunciado":"Elija la forma correcta de ingresar una imagen.",
            "respuestas":[
                {
                "a" : ["<img src='foto.jpg'>", 1],
                "b" : ["<imagen src='foto.jpg'>", 0],
                "c" : ["<img href='foto.jpg'>", 0],
                "d" : ["<background src='foto.jpg'>", 0]
                }
            ],
            "explicacion":"La forma de insertar una imagen correctamente es: <img src='foto.jpg'>"
          }
        ] 
    },
    {
        "categoria":["lenguaje"],
        "idioma":"esp",
        "validacion": 1,
        "nivel":"medio",
        "imagen":"/img/lenguaje4.jpg",
        "pregunta":[
            {
            "enunciado":"¿Qué etiqueta define un salto de línea?",
            "respuestas":[
                {
                "a" : ["br", 1],
                "b" : ["brake", 0],
                "c" : ["linebrake", 0],
                "d" : ["p", 0]
                }
            ],
            "explicacion":"La etiqueta correcta para el salto de linea es: 'br'"
          }
        ] 
    },
    {
        "categoria":["lenguaje"],
        "idioma":"esp",
        "validacion": 1,
        "nivel":"facil",
        "imagen":"/img/lenguaje5.jpg",
        "pregunta":[
            {
            "enunciado":"Elija la etiqueta que nos dá el título más grande",
            "respuestas":[
                {
                "a" : ["h6", 0],
                "b" : ["h1", 1],
                "c" : ["big", 0],
                "d" : ["title", 0]
                }
            ],
            "explicacion":"La etiqueta correcta es 'h1'"
          }
        ] 
    },
    {
        "categoria":["lenguaje"],
        "idioma":"esp",
        "validacion": 1,
        "nivel":"facil",
        "imagen":"/img/lenguaje6.jpeg",
        "pregunta":[
            {
            "enunciado":"¿Cuál es el lenguaje estándar específico para aplicar estilos de presentación a nuestras páginas web?",
            "respuestas":[
                {
                "a" : ["JavaScript", 0],
                "b" : ["CSS", 1],
                "c" : ["Flash", 0],
                "d" : ["Style", 0]
                }
            ],
            "explicacion":"CSS es el lenguaje que aplica estilos a las paginas web."
          }
        ] 
    },
    {
        "categoria":["lenguaje"],
        "idioma":"esp",
        "validacion": 1,
        "nivel":"medio",
        "imagen":"/img/lenguaje7.jpg",
        "pregunta":[
            {
            "enunciado":"¿Para que sirve el atributo 'class'?",
            "respuestas":[
                {
                "a" : ["Para clasificar el tipo de enlace ('a') que estamos definiendo: 'salto' interno, vínculo a un sitio web externo.", 0],
                "b" : ["Indica el color de la fuente que queremos aplicar, Por ejemplo: p class='red' presentaría las letras en rojo de ese párrafo.", 0],
                "c" : ["Para aplicar unos determinados estilos a los elementos que tenga la misma clase, es decir, el mismo valor en ese atributo.", 1],
                "d" : ["Todas son incorrectas", 0]
                }
            ],
            "explicacion":"Para aplicar unos determinados estilos a los elementos que tenga la misma clase, es decir, el mismo valor en ese atributo."
          }
        ] 
    },
    {
        "categoria":["lenguaje"],
        "idioma":"esp",
        "validacion": 1,
        "nivel":"medio",
        "imagen":"/img/lenguaje8.jpeg",
        "pregunta":[
            {
            "enunciado":"¿Qué función tiene el elemento 'div'?",
            "respuestas":[
                {
                "a" : ["Es un elemento divisor, y hace que el navegador muestre una línea horizontal de separación ", 0],
                "b" : ["Es un contenedor. Crea bloques, por ejemplo para diferenciar distintas secciones de una página", 1],
                "c" : ["Permite realizar una operación arirmética de división en los valores númericos de una tabla", 0],
                "d" : ["Genera una copia virtual del interior del 'div'", 0]
                }
            ],
            "explicacion":"La etiqueta <div> define una división. Esta etiqueta permite agrupar varios elementos de bloque (párrafos, encabezados, listas, tablas, divisiones, etc)."
          }
        ] 
    },
    {
        "categoria":["lenguaje"],
        "idioma":"esp",
        "validacion": 1,
        "nivel":"medio",
        "imagen":"/img/lenguaje9.jpg",
        "pregunta":[
            {
            "enunciado":"¿Qué está mal en esta regla de estilo?: .cuadro { border: 1px blue dotted padding: 10px 5px; }",
            "respuestas":[
                {
                "a" : ["Falta un ';' (punto y coma) al final de la declaración del estilo 'border'", 1],
                "b" : ["Falta una ',' (coma) entre los dos valores de la propiedad padding (10px 5px)", 0],
                "c" : ["No se puede poner un '.' (punto) al inicio de una declaración (antes de la palabra 'cuadro)'", 0],
                "d" : ["No le falta nada, esta bien hecha la regla de estilo", 0]
                }
            ],
            "explicacion":"Falta un ';' (punto y coma) al final de la declaración del estilo 'border'"
          }
        ] 
    },
    {
        "categoria":["lenguaje"],
        "idioma":"esp",
        "validacion": 1,
        "nivel":"facil",
        "imagen":"/img/lenguaje10.jpg",
        "pregunta":[
            {
            "enunciado":"¿En qué lugar se ejecuta el código PHP?",
            "respuestas":[
                {
                "a" : ["En el servidor", 1],
                "b" : ["En el cliente (ordenador del usuario)", 0],
                "c" : ["En ambos", 0],
                "d" : ["En ninguno", 0]
                }
            ],
            "explicacion":"PHP se ejecuta en el servidor, allí, los scripts de PHP generan el código HTML que se envía después al navegador."
          }
        ] 
    },
    {
        "categoria":["lenguaje"],
        "idioma":"esp",
        "validacion": 1,
        "nivel":"dificil",
        "imagen":"/img/lenguaje11.jpg",
        "pregunta":[
            {
            "enunciado":"¿Cuál de estas instrucciones está correctamente escrita en PHP?",
            "respuestas":[
                {
                "a" : ["if (a=0) print a", 0],
                "b" : ["if (a==0) { echo ok }", 0],
                "c" : ["if (a==0): print a;", 0],
                "d" : ["if (a==0) echo “hola mundo”;", 1]
                }
            ],
            "explicacion":"La instruccion correcta es: if (a==0) echo “hola mundo”;"
          }
        ] 
    },
    {
        "categoria":["lenguaje"],
        "idioma":"esp",
        "validacion": 1,
        "nivel":"medio",
        "imagen":"/img/lenguaje12.jpg",
        "pregunta":[
            {
            "enunciado":"Dos de las formas de pasar los parámetros entre páginas PHP son:",
            "respuestas":[
                {
                "a" : ["Require e Include", 0],
                "b" : ["Get y Put", 0],
                "c" : ["Post y Get", 1],
                "d" : ["Into e Include", 0]
                }
            ],
            "explicacion":"El paso de parámetros por GET se realiza a través de la propia dirección URL, mientras que el paso por POST oculta esas variables, por lo que es más seguro."
          }
        ] 
    },
    {
        "categoria":["lenguaje"],
        "idioma":"esp",
        "validacion": 1,
        "nivel":"medio",
        "imagen":"/img/lenguaje13.png",
        "pregunta":[
            {
            "enunciado":"Un array es...",
            "respuestas":[
                {
                "a" : ["Un conjunto de elementos", 1],
                "b" : ["Un conjunto de caracteres alfanuméricos", 0],
                "c" : ["Un sistema para convertir una variable de texto en un número", 0],
                "d" : ["Todas son correctas", 0]
                }
            ],
            "explicacion":"Un array es lo que también se conoce de otras formas como vector o arreglo, y consiste en un conjunto de elementos, a los que podremos acceder mediante su posición en el conjunto"
          }
        ] 
    },
    {
        "categoria":["lenguaje"],
        "idioma":"esp",
        "validacion": 1,
        "nivel":"medio",
        "imagen":"/img/lenguaje14.jpg",
        "pregunta":[
            {
            "enunciado":"¿Cómo se define una variable de tipo string en PHP?",
            "respuestas":[
                {
                "a" : ["char str;", 0],
                "b" : ["string str;", 0],
                "c" : ["dim str;", 0],
                "d" : ["En PHP no se define el tipo de las variables explícitamente", 1]
                }
            ],
            "explicacion":"En PHP no es necesario definir el tipo de la variable, sino que es capaz de modificar los tipos según la operación a la que sometamos a la variable."
          }
        ] 
    },
    {
        "categoria":["lenguaje"],
        "idioma":"esp",
        "validacion": 1,
        "nivel":"dificil",
        "imagen":"/img/lenguaje15.png",
        "pregunta":[
            {
            "enunciado":"Tenemos el siguiente código: $a='10'; $b=$a + 2; ¿Cuál será el valor de $b?",
            "respuestas":[
                {
                "a" : ["'12'", 0],
                "b" : ["12", 1],
                "c" : ["'102'", 0],
                "d" : ["102", 0]
                }
            ],
            "explicacion":"PHP internamente convierte la cadena “10” a un tipo numérico para sumarla con el entero 2, con lo que el resultado es el entero 12 (tipo entero)"
          }
        ] 
    },
    {
        "categoria":["lenguaje"],
        "idioma":"esp",
        "validacion": 1,
        "nivel":"facil",
        "imagen":"/img/lenguaje16.png",
        "pregunta":[
            {
            "enunciado":"¿Que es Jquery?",
            "respuestas":[
                {
                "a" : ["Un editor de texto.", 0],
                "b" : ["Es una biblioteca de Javascript que simplifica la interacción con documentos HTML. ", 1],
                "c" : ["Es un software de cálculos. ", 0],
                "d" : ["Es un software de código abierto para manipular elementos del CORE.", 0]
                }
            ],
            "explicacion":""
          }
        ] 
    },
    {
        "categoria":["lenguaje"],
        "idioma":"esp",
        "validacion": 1,
        "nivel":"medio",
        "imagen":"/img/lenguaje17.jpg",
        "pregunta":[
            {
            "enunciado":"La llamada al código Javascript debe colocarse en:",
            "respuestas":[
                {
                "a" : ["La sección Body de la página", 0],
                "b" : ["Antes de la etiqueta HTML", 0],
                "c" : ["Puede colocarse en la sección Head o en Body", 1],
                "d" : ["No se debe colocar en ninguna de estas opciones", 0]
                }
            ],
            "explicacion":"Puede colocarse en ambas secciones. Un consejo para no entorpecer la visualización de la página en el navegador del usuario es colocarlo al final de la sección Body para que sea lo último que se cargue. "
          }
        ] 
    },
    {
        "categoria":["lenguaje"],
        "idioma":"esp",
        "validacion": 1,
        "nivel":"medio",
        "imagen":"/img/lenguaje18.jpg",
        "pregunta":[
            {
            "enunciado":"¿Cuál es la instrucción usada para devolver un valor en una función de JavaScript?",
            "respuestas":[
                {
                "a" : ["Send", 0],
                "b" : ["Return", 1],
                "c" : ["Value", 0],
                "d" : ["End", 0]
                }
            ],
            "explicacion":"Se ha de poner return para devolver una funcion en JS."
          }
        ] 
    },
    {
        "categoria":["lenguaje"],
        "idioma":"esp",
        "validacion": 1,
        "nivel":"medio",
        "imagen":"/img/lenguaje19.jpg",
        "pregunta":[
            {
            "enunciado":"Para terminar las instrucciones en Javascript se utiliza:",
            "respuestas":[
                {
                "a" : ["Un punto y coma", 0],
                "b" : ["Un punto y coma o un salto de línea", 1],
                "c" : ["La sentencia End", 0],
                "d" : ["Todas son correctas", 0]
                }
            ],
            "explicacion":"Normalmente nos puede valer con ir pulsando intro, pero para asegurarnos de no tener errores y para tener un código más legible, es recomendable usar punto y coma."
          }
        ] 
    },
    {
        "categoria":["lenguaje"],
        "idioma":"esp",
        "validacion": 1,
        "nivel":"medio",
        "imagen":"/img/lenguaje20.jpg",
        "pregunta":[
            {
            "enunciado":"Para concatenar cadenas de caracteres en Javascript se usa el carácter:",
            "respuestas":[
                {
                "a" : ["& (ampersand)", 0],
                "b" : [". (punto)", 0],
                "c" : ["* (por) ", 0],
                "d" : ["+ (más)", 1]
                }
            ],
            "explicacion":"Una cadena de ejemplo sería: “hola” + “a” + “todos”, que daría lugar a la cadena “hola a todos”"
          }
        ] 
    }
] 
  //Conectar a la base de datos
  db = connect("localhost:27017/admin");
  
  //Introducir usuario y contraseña
  //db.auth('almi', 'Almi123');
  
  //Seleccionar la base de datos
  db = db.getSiblingDB('photoplay');
  
  //Insertar
  db.preguntas.insert(espanol);

