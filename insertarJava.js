
//Crear variable games con registros
var espanol = [
    {
      "categoria":["java"],
      "idioma":"esp",
      "validacion": 1,
      "nivel":"medio",
      "imagen":"/img/java1.jpg",
      "pregunta":[
          {
          "enunciado":"¿Cuál es la descripción que crees que define mejor el concepto 'clase' en la programación orientada a objetos?",
          "respuestas":[
              {
              "a" : ["Es un concepto similar al de 'array'.", 0],
              "b" : ["Es un tipo particular de variable.", 0],
              "c" : ["Es un modelo o plantilla a partir de la cual creamos objetos.", 1],
              "d" : ["Es una categoria de datos ordenada secuencialmente.", 0]
              }
          ],
          "explicacion":"Una clase es una herramienta que tenemos para modelar objetos de programación, lograr que se comporten como queremos y hacer tantas copias de estos como necesitemos."
        }
      ]
    },
    {
      "categoria":["java"],
      "idioma":"esp",
      "validacion": 1,
      "nivel":"facil",
      "imagen":"/img/java2.jpg",
      "pregunta":[
          {
          "enunciado":"¿Qué elementos crees que definen a un objeto?",
          "respuestas":[
              {
              "a" : ["Sus cardinalidad y su tipo", 0],
              "b" : ["Sus atributos y sus métodos", 1],
              "c" : ["La forma en que establece comunicación e intercambia mensajes", 0],
              "d" : ["Su interfaz y los eventos asociados", 0]
              }
          ],
          "explicacion":"Los objetos integran, a diferencia de los métodos procedurales, tanto los procedimientos como las variables y datos referentes al objeto."
        }
      ]
    },
    {
      "categoria":["java"],
      "idioma":"esp",
      "validacion": 1,
      "nivel":"medio",
      "imagen":"/img/java3.jpg",
      "pregunta":[
          {
          "enunciado":"¿Qué código de los siguientes tiene que ver con la herencia?",
          "respuestas":[
              {
              "a" : ["public class Componente extends Producto", 1],
              "b" : ["public class Componente inherit Producto", 0],
              "c" : ["public class Componente implements Producto", 0],
              "d" : ["public class Componente belong to Producto", 0]
              }
          ],
          "explicacion":"Para declarar la herencia en Java usamos la palabra clave extends."
        }
      ]
    },
    {
      "categoria":["java"],
      "idioma":"esp",
      "validacion": 1,
      "nivel":"facil",
      "imagen":"/img/java4.jpg",
      "pregunta":[
          {
          "enunciado":"¿Qué significa instanciar una clase?",
          "respuestas":[
              {
              "a" : ["Duplicar una clase", 1],
              "b" : ["Eliminar una clase", 0],
              "c" : ["Crear un objeto a partir de la clase", 0],
              "d" : ["Conectar dos clases entre sí", 0]
              }
          ],
          "explicacion":"Instanciar una clase significa crear un objeto que tiene todas las características (propiedades y métodos) definidos en una clase."
        }
      ]
    },
    {
      "categoria":["java"],
      "idioma":"esp",
      "validacion": 1,
      "nivel":"medio",
      "imagen":"/img/java5.jpg",
      "pregunta":[
          {
          "enunciado":" En Java, ¿a qué nos estamos refiriendo si hablamos de 'Swing'?",
          "respuestas":[
              {
              "a" : ["Una función utilizada para intercambiar valores", 0],
              "b" : ["Es el sobrenombre de la versión 1.3 del JDK", 0],
              "c" : ["Un framework específico para Android", 0],
              "d" : ["Una librería para construir interfaces gráficas", 1]
              }
          ],
          "explicacion":"Swing es una biblioteca de clases que permite crear interfaces gráficas de usuario en Java."
        }
      ]
    },
    {
      "categoria":["java"],
      "idioma":"esp",
      "validacion": 1,
      "nivel":"facil",
      "imagen":"/img/java6.jpg",
      "pregunta":[
          {
          "enunciado":"¿Qué es Eclipse?",
          "respuestas":[
              {
              "a" : ["Una libreria de Java", 0],
              "b" : ["Una versión de Java especial para servidores", 0],
              "c" : ["Un IDE para desarrollar aplicaciones", 1],
              "d" : ["Ninguna de las anteriores", 0]
              }
          ],
          "explicacion":"eclipse es el entorno de desarrollo de java."
        }
      ]
    },
    {
      "categoria":["java"],
      "idioma":"esp",
      "validacion": 1,
      "nivel":"medio",
      "imagen":"/img/java7.jpg",
      "pregunta":[
          {
          "enunciado":"¿Qué es el bytecode en Java?",
          "respuestas":[
              {
              "a" : ["El formato de intercambio de datos", 0],
              "b" : ["El formato que obtenemos tras compilar un fuente .java", 1],
              "c" : ["Un tipo de variable", 0],
              "d" : ["Un depurador de código", 0]
              }
          ],
          "explicacion":"El bytecode Java se encuentra dentro del archivo de extensión .class y es el tipo de instrucciones que la máquina virtual Java espera recibir, para posteriormente ser compiladas a lenguaje de máquina, mediante un compilador JIT a la hora de su ejecución."
        }
      ]
    },
    {
      "categoria":["java"],
      "idioma":"esp",
      "validacion": 1,
      "nivel":"medio",
      "imagen":"/img/java8.jpg",
      "pregunta":[
          {
          "enunciado":"¿Qué código asociarías a una Interfaz en Java?",
          "respuestas":[
              {
              "a" : ["public class Componente interface Product", 0],
              "b" : ["Componente cp = new Componente (interfaz)", 0],
              "c" : ["public class Componente implements Printable", 1],
              "d" : ["Componente cp = new Componente.interfaz", 0]
              }
          ],
          "explicacion":"Asi lo manda Robert :3."
        }
      ]
    },
    {
      "categoria":["java"],
      "idioma":"esp",
      "validacion": 1,
      "nivel":"medio",
      "imagen":"/img/java9.jpg",
      "pregunta":[
          {
          "enunciado":"¿Qué significa sobrecargar (overload) un método?",
          "respuestas":[
              {
              "a" : ["Editarlo para modificar su comportamiento", 0],
              "b" : ["Editarlo para modificar su comportamiento", 0],
              "c" : ["Crear un método con el mismo nombre pero diferentes argumentos", 1],
              "d" : ["Añadirle funcionalidades a un método", 0]
              }
          ],
          "explicacion":"Overloading significa que un mismo método tiene diferentes firmas."
        }
      ]
    },
    {
      "categoria":["java"],
      "idioma":"esp",
      "validacion": 1,
      "nivel":"facil",
      "imagen":"/img/java10.jpg",
      "pregunta":[
          {
          "enunciado":"¿Qué es una excepción?",
          "respuestas":[
              {
              "a" : ["Un error que lanza un método cuando algo va mal", 1],
              "b" : ["Un objeto que no puede ser instanciado", 0],
              "c" : ["Un bucle que no finaliza", 0],
              "d" : ["Un tipo de evento muy utilizado al crear interfaces", 0]
              }
          ],
          "explicacion":"Las excepciones permiten que un método informe al código que lo ha invocado acerca de algún error o situación anómala que se haya producido durante su ejecución."
        }
      ]
    },
    {
      "categoria":["java"],
      "idioma":"esp",
      "validacion": 1,
      "nivel":"dificil",
      "imagen":"/img/java11.jpg",
      "pregunta":[
          {
          "enunciado":"En la precedencia de operadores cual va antes",
          "respuestas":[
              {
              "a" : ["^", 0],
              "b" : ["+ -", 1],
              "c" : ["|", 0],
              "d" : ["<<", 0]
              }
          ],
          "explicacion":" Primeramente proceden los unarios, luego los aritméticos, después los de bits, posteriormente los relacionales, detrás vienen los booleanos y por último el operador de asignación."
        }
      ]
    },
    {
      "categoria":["java"],
      "idioma":"esp",
      "validacion": 1,
      "nivel":"dificil",
      "imagen":"/img/java12.jpg",
      "pregunta":[
          {
          "enunciado":"En la precedencia de operadores cual va antes",
          "respuestas":[
              {
              "a" : ["* / %", 0],
              "b" : ["+ -", 0],
              "c" : ["++expresion", 1],
              "d" : ["<<", 0]
              }
          ],
          "explicacion":" Primeramente proceden los unarios, luego los aritméticos, después los de bits, posteriormente los relacionales, detrás vienen los booleanos y por último el operador de asignación."
        }
      ]
    },
    {
      "categoria":["java"],
      "idioma":"esp",
      "validacion": 1,
      "nivel":"medio",
      "imagen":"/img/java13.jpg",
      "pregunta":[
          {
          "enunciado":"¿Cuál es la precisión de un tipo de dato long?",
          "respuestas":[
              {
              "a" : ["8 bit", 0],
              "b" : ["16 bit", 0],
              "c" : ["32 bit", 0],
              "d" : ["64 bit", 1]
              }
          ],
          "explicacion":"Es un tipo de dato para almacenar números en coma flotante con doble precisión de 64 bits."
        }
      ]
    },
    {
      "categoria":["java"],
      "idioma":"esp",
      "validacion": 1,
      "nivel":"medio",
      "imagen":"/img/java14.jpg",
      "pregunta":[
          {
          "enunciado":"El operador break en una estructura switch se utiliza para",
          "respuestas":[
              {
              "a" : ["Terminar el programa", 0],
              "b" : ["Para pasar a la siguiente evaluación", 0],
              "c" : ["Finalizar la evaluación de condiciones", 1],
              "d" : ["Ninguna de las respuestas es correcta", 0]
              }
          ],
          "explicacion":"Si aparece break, se termina la ejecución de la estructura switch."
        }
      ]
    },
    {
      "categoria":["java"],
      "idioma":"esp",
      "validacion": 1,
      "nivel":"medio",
      "imagen":"/img/java15.jpg",
      "pregunta":[
          {
          "enunciado":"¿Qué modificador de acceso hace que una clase sea pública en Java?",
          "respuestas":[
              {
              "a" : ["private", 0],
              "b" : ["protected", 0],
              "c" : ["free", 0],
              "d" : ["public", 1]
              }
          ],
          "explicacion":"Las clases tienen uno de los modificadores de acceso public"
        }
      ]
    },
    {
      "categoria":["java"],
      "idioma":"esp",
      "validacion": 1,
      "nivel":"medio",
      "imagen":"/img/java16.jpg",
      "pregunta":[
          {
          "enunciado":"Los programas en Java se pueden construir mediante estructuras repetitivas, selectivas y ...",
          "respuestas":[
              {
              "a" : ["secuenciales", 1],
              "b" : ["algorítmicas", 0],
              "c" : ["de cálculo", 0],
              "d" : ["logarítmicas", 0]
              }
          ],
          "explicacion":"Robert así lo manda :3"
        }
      ]
    },
    {
      "categoria":["java"],
      "idioma":"esp",
      "validacion": 1,
      "nivel":"facil",
      "imagen":"/img/java17.jpg",
      "pregunta":[
          {
          "enunciado":"¿Es Java un lenguaje orientado a objetos?",
          "respuestas":[
              {
              "a" : ["si", 1],
              "b" : ["no", 0],
              "c" : ["depende del uso", 0],
              "d" : ["depende del compilador", 0]
              }
          ],
          "explicacion":"Robert así lo manda :3"
        }
      ]
    },
    {
      "categoria":["java"],
      "idioma":"esp",
      "validacion": 1,
      "nivel":"medio",
      "imagen":"/img/java18.jpg",
      "pregunta":[
          {
          "enunciado":"Para que una condición con el operador || sea verdadera se tiene que cumplir que...",
          "respuestas":[
              {
              "a" : ["Cualquier operando sea true", 1],
              "b" : ["Todos los operandos sean true", 0],
              "c" : ["Al menos dos operados sean true", 0],
              "d" : ["Las tres respuestas son correctas", 0]
              }
          ],
          "explicacion":"Robert así lo manda :3"
        }
      ]
    },
    {
      "categoria":["java"],
      "idioma":"esp",
      "validacion": 1,
      "nivel":"medio",
      "imagen":"/img/java19.jpg",
      "pregunta":[
          {
          "enunciado":"¿Cual es el operador resto en Java?",
          "respuestas":[
              {
              "a" : ["/", 0],
              "b" : ["%", 1],
              "c" : ["mod", 0],
              "d" : ["|", 0]
              }
          ],
          "explicacion":"Robert así lo manda :3"
        }
      ]
    },
    {
      "categoria":["java"],
      "idioma":"esp",
      "validacion": 1,
      "nivel":"medio",
      "imagen":"/img/java20.jpg",
      "pregunta":[
          {
          "enunciado":"¿Cuál es la precisión de un tipo de dato short?",
          "respuestas":[
              {
              "a" : ["8 bit", 0],
              "b" : ["16 bit", 1],
              "c" : ["32 bit", 0],
              "d" : ["64 bit", 0]
              }
          ],
          "explicacion":"short. Representa un tipo de dato de 16 bits con signo."
        }
      ]
    },
    {
      "categoria":["java"],
      "idioma":"esp",
      "validacion": 1,
      "nivel":"medio",
      "imagen":"/img/java21.jpg",
      "pregunta":[
          {
          "enunciado":"Menciona cual es la estructura de un comentario para aun sola línea",
          "respuestas":[
              {
              "a" : [" /* */", 0],
              "b" : ["//", 1],
              "c" : ["%/", 0],
              "d" : ["/** * /", 0]
              }
          ],
          "explicacion":"asi lo manda Robert :3"
        }
      ]
    },
    {
      "categoria":["java"],
      "idioma":"esp",
      "validacion": 1,
      "nivel":"medio",
      "imagen":"/img/java22.jpg",
      "pregunta":[
          {
          "enunciado":" Menciona cual es la estructura de un comentario para una o varias línea",
          "respuestas":[
              {
              "a" : [" /* */", 1],
              "b" : ["//", 0],
              "c" : ["%/", 0],
              "d" : ["/** * /", 0]
              }
          ],
          "explicacion":"asi lo manda Robert :3"
        }
      ]
    },
    {
      "categoria":["java"],
      "idioma":"esp",
      "validacion": 1,
      "nivel":"medio",
      "imagen":"/img/java23.jpg",
      "pregunta":[
          {
          "enunciado":"Menciona cual es la estructura de un comentario de documento, de una o más líneas",
          "respuestas":[
              {
              "a" : [" /* */", 0],
              "b" : ["//", 0],
              "c" : ["%/", 0],
              "d" : ["/** * /", 1]
              }
          ],
          "explicacion":"asi lo manda Robert :3"
        }
      ]
    },
    {
      "categoria":["java"],
      "idioma":"esp",
      "validacion": 1,
      "nivel":"medio",
      "imagen":"/img/java24.jpg",
      "pregunta":[
          {
          "enunciado":"Cual de estas son palabras reservadas de java",
          "respuestas":[
              {
              "a" : ["cast future generic", 1],
              "b" : ["continue for Boolean", 0],
              "c" : ["default goto null", 0],
              "d" : ["Implements Public Extends", 0]
              }
          ],
          "explicacion":" Las palabras reservadas son identificadores, pero como su nombre indica, estas palabras están reservadas, y no se pueden usar como identificadores de usuario."
        }
      ]
    },
    {
      "categoria":["java"],
      "idioma":"esp",
      "validacion": 1,
      "nivel":"medio",
      "imagen":"/img/java25.jpg",
      "pregunta":[
          {
          "enunciado":"¿Qué es una clase en java?",
          "respuestas":[
              {
              "a" : ["Es un conjunto de declaraciones de funciones", 0],
              "b" : ["Es una agrupación que termina una cadena de herencia.", 1],
              "c" : ["Es una agrupación de datos y de funciones", 0],
              "d" : ["Es conjunto de variables y funciones relacionadas con esas variables.", 0]
              }
          ],
          "explicacion":"Una clase es una plantilla que define la forma de un objeto; en ella se agrupan datos y métodos que operarán sobre esos datos."
        }
      ]
    },
    {
      "categoria":["java"],
      "idioma":"esp",
      "validacion": 1,
      "nivel":"medio",
      "imagen":"/img/java26.jpg",
      "pregunta":[
          {
          "enunciado":"Al menos cuantos métodos tiene una clase abstract",
          "respuestas":[
              {
              "a" : ["cinco", 0],
              "b" : ["uno", 0],
              "c" : ["dos", 1],
              "d" : ["28", 0]
              }
          ],
          "explicacion":"Una clase abstracta puede contener métodos no-abstractos pero al menos uno de los métodos debe ser declarado abstracto."
        }
      ]
    },
    {
      "categoria":["java"],
      "idioma":"esp",
      "validacion": 1,
      "nivel":"medio",
      "imagen":"/img/java27.jpg",
      "pregunta":[
          {
          "enunciado":"¿Cómo se llama la clase que termina una cadena de herencia?",
          "respuestas":[
              {
              "a" : ["Public", 0],
              "b" : ["Abstract", 0],
              "c" : ["Static", 0],
              "d" : ["final", 1]
              }
          ],
          "explicacion":"Y punto final."
        }
      ]
    },
    {
      "categoria":["java"],
      "idioma":"esp",
      "validacion": 1,
      "nivel":"medio",
      "imagen":"/img/java28.jpg",
      "pregunta":[
          {
          "enunciado":"¿Cómo se especifica las clases que tiene una súper clase?",
          "respuestas":[
              {
              "a" : ["Interface", 0],
              "b" : ["implements", 0],
              "c" : ["Extends", 1],
              "d" : ["Object", 0]
              }
          ],
          "explicacion":"Y punto final."
        }
      ]
    },
    {
      "categoria":["java"],
      "idioma":"esp",
      "validacion": 1,
      "nivel":"medio",
      "imagen":"/img/java29.jpg",
      "pregunta":[
          {
          "enunciado":"¿Qué es una interface?",
          "respuestas":[
              {
              "a" : ["Una súper clase", 0],
              "b" : ["Es un conjunto de declaraciones de funciones", 1],
              "c" : ["Una herencia múltiple", 0],
              "d" : ["Un fichero public", 0]
              }
          ],
          "explicacion":"Una interfaz en Java es una colección de métodos abstractos y propiedades constantes."
        }
      ]
    },
    {
      "categoria":["java"],
      "idioma":"esp",
      "validacion": 1,
      "nivel":"medio",
      "imagen":"/img/java30.jpg",
      "pregunta":[
          {
          "enunciado":"¿Cómo se debe llamar el fichero con extensión .java?",
          "respuestas":[
              {
              "a" : ["El fichero se debe llamar como atributo", 0],
              "b" : ["El fichero se debe llamar como la clase interface", 0],
              "c" : ["El fichero se debe llamar como la clase object", 0],
              "d" : ["El fichero de debe llamar como la clase public", 1]
              }
          ],
          "explicacion":"En caso de haber una clase definida public esta clase se debe de llamar obligatoriamente igual que el nombre del fichero, respetando mayúsculas y minúsculas, pero sin la extensión del fichero."
        }
      ]
    }
]
  //Conectar a la base de datos
  db = connect("localhost:27017/admin");

  //Introducir usuario y contraseña
  //db.auth('almi', 'Almi123');

  //Seleccionar la base de datos
  db = db.getSiblingDB('photoplay');

  //Insertar
  db.preguntas.insert(espanol);
